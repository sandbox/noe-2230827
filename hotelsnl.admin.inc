<?php

/**
 * @file
 * The admin form handling of the hotels.nl module.
 */

/**
 * Builds the settings form.
 */
function hotelsnl_settings_form($form, &$form_state) {
  $form['hotelsnl_affiliate'] = array(
    '#type' => 'textfield',
    '#title' => t('Affiliate code'),
    '#description' => t('The affiliate code you got when you signed up'),
    '#default_value' => variable_get('hotelsnl_affiliate'),
  );

  $form['hotelsnl_password'] = array(
    '#type' => 'password', // @TODO MAke this a password field later.
    '#title' => t('Password'),
    '#description' => t('The password you got when you signed up'),
    '#default_value' => variable_get('hotelsnl_password'),
  );

  return system_settings_form($form);
}

/**
 * Builds the settings test form.
 */
function hotelsnl_settings_test($form, &$form_state) {
  $form = array();

  $form['hotelsnl_hotel_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Hotels.nl hotel ID'),
    '#description' => t('Write a hotels.nl hotel id in this form'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Look up hotel'),
  );

  if (isset($form_state['hotelsnl_hotel'])) {
    $form['hotelsnl_hotel'] = array(
      '#type' => 'fieldset',
      '#title' => t('Result'),
      '#collapsible' => FALSE,
    );

    $form['hotelsnl_hotel'] = array(
      '#markup' => theme(
        'hotelsnl_hotel',
        array('hotel' => $form_state['hotelsnl_hotel'])
      ),
      '#weight' => 5,
    );

    $form['hotelsnl_hotel']['details'] = array(
      '#type' => 'fieldset',
      '#title' => t('Details'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 10,
    );

    $form['hotelsnl_hotel']['details']['data'] = array(
      '#markup' => '<pre><small>'
        . print_r($form_state['hotelsnl_hotel'], TRUE)
        . '</small></pre>',
    );
  }

  return $form;
}

/**
 * Validates the test form.
 */
function hotelsnl_settings_test_validate($form, &$form_state) {
  if (!preg_match('/^[0-9]+$/', $form_state['values']['hotelsnl_hotel_id'])) {
    form_set_error('hotelsnl_hotel_id', t('That is not a valid hotels.nl hotel id'));
  }
}

/**
 * Submits the test form.
 */
function hotelsnl_settings_test_submit($form, &$form_state) {
  $data = hotelsnl_fetch_hotel($form_state['values']['hotelsnl_hotel_id']);

  $form_state['hotelsnl_hotel'] = $data;

  $form_state['rebuild'] = TRUE;
}
