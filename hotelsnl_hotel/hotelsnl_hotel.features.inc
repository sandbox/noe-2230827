<?php
/**
 * @file
 * hotelsnl_hotel.features.inc
 */

/**
 * Implements hook_node_info().
 */
function hotelsnl_hotel_node_info() {
  $items = array(
    'hotel' => array(
      'name' => t('hotel'),
      'base' => 'node_content',
      'description' => t('A hotel'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
